import React, {Component} from "react";

export default class Header extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="header-wrap">
                <div className="container">
                    <div className="logo"/>
                    <div className="menu-items">
                        <div className="name">Welcome, {this.props.userDetails[0].username}</div>
                        <div className="mitem" onClick={()=>{
                            if(window.confirm("Are you sure?")){
                                window.sessionStorage.removeItem("ud");
                                window.sessionStorage.removeItem("loggedIn");
                                console.log(this.props);
                                this.props.history.goBack();
                            }
                        }}>
                            SIGN OUT
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}