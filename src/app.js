import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.scss";
import "./images/favicon.ico";
import { BrowserRouter } from "react-router-dom";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import Home from "./containers/Home";

ReactDOM.render(
<BrowserRouter>
    <MuiThemeProvider>
    <Home/>
    </MuiThemeProvider>
</BrowserRouter>, document.getElementById("root"));