import React, { Component } from "react";
import Header from "../common/Header";
import { Switch, Route } from "react-router-dom";
import LoginPage from "./Login";
import TagImage from "./TagImage";

export default class Home extends Component {
    render() {
        return (
            <div className="main">
                <Switch>
                    <Route exact path='/' component={LoginPage}/>
                    <Route exact path='/tag' component={TagImage}/>
                </Switch>
            </div>
        );
    }
}