import React, {Component} from "react";
import Header from "../common/Header";
import axios from "axios";
import Constants from "../common/constants";
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
import Snackbar from "material-ui/Snackbar";
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

export default class TagImage extends Component {
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress : false,
            currentImageData:null,
            snackbarMessage:"",
            snackbarOpen:false,
            tagData:[],
            open:false,
            tagmap:{},
            statData:null,
            bgSize:"100%",
            selectedTags:[]
        };

        this.originalTags = [];
        this.userDetails = [{
            username:""
        }]
        var ud = window.sessionStorage.getItem("ud");
        if(ud !== null){
            this.userDetails = JSON.parse(ud);
        }
    }

    componentDidMount(){
        var isLoggedIn = window.sessionStorage.getItem("loggedIn");
        if(isLoggedIn){
            this.getImage();
            this.getAllTags();
            this.getCurrentStats();
        } else {
            this.props.history.push("/");
        }
    }

    getCurrentStats(){
        axios({
            method:"GET",
            url:Constants.BASE_URL+"image/stats/"+this.userDetails[0]._id
        }).then((response)=>{
            this.setState({
                statData:response.data
            });
        }).catch((err)=>{
            this.setState({
                statData:null
            })
        })
    }

    getAllTags(){
        axios({
            method:"GET",
            url:Constants.BASE_URL+"tags/all"
        }).then((response)=>{
            this.setState({
                tagData:response.data
            });
        }).catch((err)=>{
            this.setState({
                tagData:[]
            })
        })
    }

    getImage(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:Constants.BASE_URL+"image/"+this.userDetails[0]._id
            }).then((response)=>{
                if(response.data.tags){
                    this.originalTags = JSON.parse(JSON.stringify(response.data.tags));
                    this.setState({
                        apiCallInProgress:false,
                        currentImageData:response.data,
                        selectedTags:[]
                    });
                } else {
                    this.setState({
                        apiCallInProgress:false,
                        currentImageData:null
                    })
                }
            }).catch((error)=>{
                this.setState({
                    apiCallInProgress:false,
                    currentImageData:null
                });
            })
        })
    }

    deleteTag(tagName){
        var stateCopy = this.state.currentImageData;
        var index = stateCopy.tags.indexOf(tagName);
        stateCopy.tags.splice(index,1);
        this.setState({
            currentImageData:stateCopy
        });
    }

    addTag(tagName){
        var stateCopy = this.state.selectedTags;
        var index = this.state.selectedTags.indexOf(tagName);
        if(index == -1){
            stateCopy.push(tagName)
        } else {
            stateCopy.splice(index,1);
        }
        this.setState({
            selectedTags:stateCopy
        })
    }

    submit(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"POST",
                url:Constants.BASE_URL+"image/"+this.userDetails[0]._id,
                data:{
                    id:this.state.currentImageData._id,
                    newTags:this.state.selectedTags,
                    modifiedBy:this.userDetails[0].username
                } 
            }).then((response)=>{
                this.setState({
                    apiCallInProgress:false,
                    snackbarMessage:"Success!",
                    snackbarOpen:true
                },()=>{
                    this.getImage();
                    this.getCurrentStats();
                })
            }).catch((err)=>{
                console.log(err);
                this.setState({
                    apiCallInProgress:false,
                    snackbarMessage:"Error!",
                    snackbarOpen:true
                })
            })
        })
    }

    passImageToAdmin(){
        axios({
            method:"POST",
            url:Constants.BASE_URL+"image/passToAdmin/"+this.userDetails[0]._id,
            data:{
                id:this.state.currentImageData._id,
                newTags:this.state.currentImageData.tags,
                modifiedBy:this.userDetails[0].username
            } 
        }).then((response)=>{
            this.setState({
                apiCallInProgress:false,
                snackbarMessage:"Image passed to admin",
                snackbarOpen:true
            },()=>{
                this.getImage();
                this.getCurrentStats();
            })
        }).catch((err)=>{
            console.log(err);
            this.setState({
                apiCallInProgress:false,
                snackbarMessage:"Error!",
                snackbarOpen:true
            })
        })   
    }

    render(){
        return(
            <div className="tag-container">
                {
					this.state.apiCallInProgress ?
					<LinearProgress mode="indeterminate" style={{height:"10px", position:"absolute",left:"0px",right:"0px",top:"70px"}}/>
					:
					null
				}
                <Header {...this.props} userDetails={this.userDetails}/>
                <div className="content-wrap">
                    <div className="image-wrap">
                    {
                        this.state.currentImageData !== null ?
                            <div 
                                className="image-bg" 
                                style={{
                                    backgroundImage:"url(" + this.state.currentImageData.url + ")",
                                    backgroundSize:this.state.bgSize
                                }}
                                onWheel={(evt)=>{
                                    evt.preventDefault();
                                    evt.stopPropagation();
                                    if(evt.deltaY > 0) {
                                        var bgs = parseInt(this.state.bgSize.substring(0,this.state.bgSize.indexOf("%")));
                                        if(bgs < 200){
                                            bgs += 1;
                                            this.setState({
                                                bgSize:bgs+"%"
                                            });
                                        }
                                        
                                    } else if(evt.deltaY < 0) {
                                        var bgs = parseInt(this.state.bgSize.substring(0,this.state.bgSize.indexOf("%")));
                                        if(bgs >= 100){
                                            bgs -= 1;
                                            this.setState({
                                                bgSize:bgs+"%"
                                            });
                                        }
                                    }   
                                }}/>
                            :
                            <div className="no-img">No Image to show</div>
                    }
                    </div>
                    <div className="form-wrap">
                        {
                            this.state.statData !== null ?
                            <div className="stat-wrap">
                                Current Stats : 
                                <div className="stats">
                                    <div className="st-item">
                                        <div className="stlbl">Total Done:</div>
                                        <div className="stval">{this.state.statData.done + this.state.statData.passedToAdmin}</div>
                                    </div>
                                    <div className="st-item">
                                        <div className="stlbl">Non - Empty Tags:</div>
                                        <div className="stval">{this.state.statData.done - this.state.statData.empty}</div>
                                    </div>
                                    <div className="st-item">
                                        <div className="stlbl">Passed to Admin:</div>
                                        <div className="stval">{this.state.statData.passedToAdmin}</div>
                                    </div>
                                </div>
                            </div>
                            :
                            null
                        }
                        {
                            this.state.currentImageData !== null ?
                            <div className="img-tags">
                                <div className="title">Tags found in this image</div>
                                {
                                    this.state.currentImageData.tags.length > 0 ?
                                    <div className="tags">
                                        {
                                            this.state.currentImageData.tags.map((_tag,index)=>{
                                                return(
                                                    <div key={index} className="tag" onClick={()=>{
                                                                this.addTag(_tag);
                                                            }}>
                                                        {_tag}
                                                        {
                                                            this.state.selectedTags.indexOf(_tag) !== -1 ?
                                                            <div className="ad"/>
                                                            :
                                                            null
                                                        }  
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                    :
                                    <div className="no-tag">No Tags in the image</div>        
                                }
                            </div>
                            :
                            <div className="no-tag">Image Data not found</div>
                        }
                        {
                            this.state.currentImageData !== null ?
                            <div className="btn-wrap">
                                <RaisedButton primary style={{display:"block",marginBottom:30}} label={(this.state.apiCallInProgress  ? "Please Wait..." : "Submit")} onClick={()=>{
                                    this.submit();
                                }}/>
                                <RaisedButton label="Reset" onClick={()=>{
                                    this.setState({
                                        selectedTags:[]
                                    });
                                }}/>

                                <RaisedButton label={"Add a tag"} style={{marginLeft:10}} onClick={()=>{
                                    this.setState({
                                        open:true
                                    });
                                }}/>
                                <RaisedButton label={"Pass to Admin"} style={{marginLeft:10}} onClick={()=>{
                                    this.passImageToAdmin();    
                                }}/>
                            </div>
                            :
                            <div className="btn-wrap">
                            <RaisedButton primary label={"Refresh"} onClick={()=>{
                                this.getImage();
                            }}/>
                        </div>
                        }
                    </div>
                </div>
                <Dialog
                    title="Choose new Tag"
                    actions={[
                        <FlatButton
                            label="Cancel"
                            onClick={()=>{
                                this.setState({
                                    open:false
                                })
                            }}
                        />,
                        <FlatButton
                            label="Add"
                            primary={true}
                            onClick={()=>{
                                var tagmapstate = this.state.tagmap;
                                var currentTags = this.state.currentImageData;
                                Object.keys(tagmapstate).map((tt)=>{
                                    if(currentTags.tags.indexOf(tagmapstate[tt]) == -1){
                                        currentTags.tags.push(tagmapstate[tt])
                                    }
                                });
                                this.setState({
                                    currentImageData:currentTags,
                                    open:false
                                });
                            }}
                        />
                    ]}
                    modal={true}
                    open={this.state.open}>
                        <div className="dialog-wrap">
                            {
                                this.state.tagData.map((tg,index)=>{
                                    return(
                                        <div className="tag-cont" key={index}>
                                            <div className="tag-lbl">{tg.category_name}</div>
                                            <div className="tagss">
                                                <RadioButtonGroup name={tg.category_name} style={{display:"flex",marginBottom:20}} onChange={(event,value)=>{
                                                    var stateCopy = this.state.tagmap;
                                                    stateCopy[tg.category_name]= value;
                                                    this.setState({
                                                        tagmap : stateCopy
                                                    });
                                                }}>
                                                {
                                                    tg.tags.map((tgval,tgindex)=>{
                                                        return(
                                                            <RadioButton
                                                                className="hello"
                                                                key={tgindex}
                                                                value={tgval}
                                                                label={tgval}/>
                                                        );
                                                    })
                                                }
                                                </RadioButtonGroup>
                                            </div>
                                        </div>
                                    );
                                })
                            }    
                        </div>
                    </Dialog>
                <Snackbar
					open={this.state.snackbarOpen}
					message={this.state.snackbarMessage}
					autoHideDuration={2000}
					onRequestClose={()=>{
						this.setState({snackbarOpen:false})
					}}/> 
            </div>
        )
    }
}