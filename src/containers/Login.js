import React, { Component } from "react";
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import Constants from "../common/constants";
import Snackbar from "material-ui/Snackbar";
import axios from "axios";
import LinearProgress from 'material-ui/LinearProgress';

export default class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            username:"",
			password:"",
			usernameError:"",
			passwordError:"",
			apiCallInProgress:false,
			snackbarOpen:false,
			snackbarMessage:""
        }
    }

    componentDidMount(){
		this.username.focus();
	}

	submitForm(){
		if(this.state.username == ""){
			this.setState({usernameError:"Username cannot be empty"});
		}
		if(this.state.password == ""){
			this.setState({passwordError:"Password cannot be empty"});
		}
		if(this.state.username != "" && this.state.password != ""){
			this.makeLoginApiCall();
		}
	}

	makeLoginApiCall(){
		this.setState({
			apiCallInProgress : true
		},()=>{
			axios({
				method:"POST",
				url: Constants.BASE_URL + "login/",
				data:{
					username:this.state.username,
					password:this.state.password
				}
			}).then((response)=>{
				this.setState({
					apiCallInProgress:false
				},()=>{
					window.sessionStorage.setItem("loggedIn",true);
					window.sessionStorage.setItem("ud",JSON.stringify(response.data))
					this.props.history.push("/tag");
				})
			}).catch((error)=>{
				console.log(error.response);
				this.setState({
					apiCallInProgress:false,
					username:"",
					password:"",
					snackbarMessage:error.response.data,
					snackbarOpen:true
				},()=>{
					this.username.focus();
				})
			});
		})
	}

    render(){
        return(
            <div className="login-container">
				{
					this.state.apiCallInProgress ?
					<LinearProgress mode="indeterminate" style={{height:"10px", position:"absolute",left:"0px",right:"0px",top:"0px"}}/>
					:
					null
				}
				<div className="login-card">
					<div className={`inner-wrap`+ (this.state.apiCallInProgress ? " fade" : "")}>
						<div className="sub">
							<img src={require("../images/logo.svg")}/>
							Image Tagging
						</div>
						<form name="login-form" onSubmit={(event)=>{
							event.preventDefault();
							event.stopPropagation();
							this.submitForm(event);
						}}>
							<TextField
								ref={node => this.username = node}
								style={{display:"block"}}
								fullWidth
								floatingLabelText="Username"
								errorText={this.state.usernameError}
								value={this.state.username}
								onChange={(event,newValue)=>{
									this.setState({
										usernameError:"",
										username:newValue
									});
								}}/>
							<TextField
								ref="password"
								fullWidth
								type="password"
								floatingLabelText="Password"
								style={{display:"block",marginTop:"20px"}}
								errorText={this.state.passwordError}
								value={this.state.password}
								onChange={(event,newValue)=>{
									this.setState({
										passwordError:"",
										password:newValue
									});
								}}/>
							<div className="btn-container">
								<RaisedButton 
									disabled={this.state.apiCallInProgress}
									label={this.state.apiCallInProgress ? "Please wait..." : "Login"}
									primary={true}
									type="submit"
									fullWidth
									onClick={(event)=>{
										event.preventDefault();
										this.submitForm();
									}}/>
							</div>
						</form>
					</div>
				</div> 
				<Snackbar
					open={this.state.snackbarOpen}
					message={this.state.snackbarMessage}
					autoHideDuration={4000}
					onRequestClose={()=>{
						this.setState({snackbarOpen:false})
					}}/> 
			</div>
        );
    }
}